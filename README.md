# jitsi-meet

## Description
This is a set of Ansible-playbooks that build a secure medical interview application on SURF Research Cloud
The application is based on Jitsi-Meet.
Jitsi is considered to be safe by design. We will take efforts to harden it and test it.
Recorded interview data will be encrypted before it is saved to persistent storage.

## Project status
This project is under initial construction.